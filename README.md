# Recency Weighted Average

The exponential moving average is a weighted average of the last n races, where the weighting decreases exponentially with each previous race/period. In other words, the formula gives recent race more weight than past races.