(Load "c:/Users/mike/Dropbox/RWA-CSV/rwa.lisp")
;(ql:quickload :array-operations)
;(use-package :array-operations)
(ql:quickload :trivia)
(use-package :trivia)

;(setf *the_csv_file* (getCSV2))
;(setf *the_csv_file_length* (length *the_csv_file*))
;(setf *sorted-d-j-p-h* (sort-d-j-p-h(d-p-h->d-j-p-h *the_csv_file*))) ;; (subseq (getcsv2) 1)
;(setf *dates* (get-column *sorted-d-j-p-h* 0))
;(setf *julians* (get-column *sorted-d-j-p-h* 1))
;(setf *positions* (get-column *sorted-d-j-p-h* 2))
;(setf *horses* (get-column *sorted-d-j-p-h* 3))
;(setf *days_since_LS* (array-operations:stack 0 #(0.0) (if-and-2-func #'are-side-by-side-elem-eq (cutBehind *horses* 1) 1 #'diff-julian-dates (cutFront *julians* 1))))

(defun diff-horses (seq_horses)
  (let ((new_array (make-array (- (length seq_horses) 1) :element-type 'single-float))
        (elements (subseq seq_horses 1))
        (comparand (aref seq_horses 0)) )
    (dotimes (i (length elements) new_array)
      (let ((element  (aref elements i)))
        (if (= comparand element)
            (setf (aref new_array i) 1.0)
            (setf (aref new_array i) 0.0))))))

(defun last-start-days-ago-array (&rest lsd_n)
  "assigns parameters to array where elements: last_start_days_ago_1st, last_start_2nd,...,last_start_17th"
  (let ((numargs (length lsd_n))) (make-array  `(,numargs)  :element-type 'single-float :initial-contents lsd_n)))


(defun compose (f g) (lambda (x) (funcall f (funcall g x))))

(defun get-row-subseq (row col_seq diff-reverse-compose-fn)
  (trivia:match row
    (0    (vector))    
    ((trivia:guard r (< 0 r 17))
                                       
     (funcall diff-reverse-compose-fn (subseq col_seq 0 r))
     )
    ((trivia:guard r (>= r 17))
                                        
     (funcall diff-reverse-compose-fn (subseq col_seq (- r 17) r))
     )
    (_ (print "error getting row of horses or julians in get-row-subseq "))))

;; days_since_LS
;; starts from row 2
;; #(15.0 0.0 0.0 8.0 0.0 0.0 15.0 0.0 0.0 15.0 0.0 0.0 0.0 15.0 0.0 0.0)
 ; default num_last_start_calc 16
(defun last-start-calc(&optional (num_last_start_calc 16) horses julians days_since_LS_elem)

  (let ((new_row_array (make-array '17 :element-type 'single-float)))
    (dotimes (col_num (- num_last_start_calc 1) new_row_array)
      (trivia:match col_num
        (0
         (setf (aref new_row_array col_num)
               (1st_last_start_element 
                                             (aref horses col_num)
                                             (aref julians col_num)
                                             days_since_LS_elem)))
         ((trivia:guard col (< 0 col 17))
          (setf (aref new_row_array col_num)
                (Nth_last_start_element
                 (aref horses col_num)
                 (aref julians col_num)
                 (aref new_row_array (- col_num 1)))))
        (_ (print "error calculating last_start row"))))))

;; Start from row 1
(defun last-start-days-ago-row (row horses julians days_since_LS);; aka lsd_n
  (trivia:match row
    (1
     (make-array '17 :element-type 'single-float))
    ((trivia:guard row (< 1 row 17))
     (last-start-calc row                      
                      (get-row-subseq row horses (compose #'diff-horses #'nreverse))
                      (get-row-subseq row julians (compose #'nreverse #'diff-julian-dates))                     
                      (aref days_since_LS (- row 2))))
    
    ((trivia:guard row (>= row 17))
     (last-start-calc
      row
      (get-row-subseq row horses (compose #'diff-horses #'nreverse))
      (get-row-subseq row julians (compose #'nreverse #'diff-julian-dates))      
      (aref days_since_LS (- row 1))
      ))
    
    (_   (print "Either out of bounds; must be between row 1 and row n OR pattern error"))))

;; https://rosettacode.org/wiki/Element-wise_operations#Common_Lisp
(defun element-wise-matrix (fn A B)
  (let* ((len (array-total-size A))
         (m   (car (array-dimensions A)))
         (n   (cadr (array-dimensions A)))
         (C   (make-array `(,m ,n) :element-type 'single-float)))
    
    (loop for i from 0 to (1- len) do
          (setf (row-major-aref C i) 
                (funcall fn
                         (row-major-aref A i)
                         (row-major-aref B i))))
    C))

;; A.+B, A.-B, A.*B, A./B, A.^B.
(defun m+ (A B) (element-wise-matrix #'+ A B))
(defun m- (A B) (element-wise-matrix #'- A B))
(defun m* (A B) (element-wise-matrix #'* A B))
(defun m/ (A B) (element-wise-matrix #'/ A B))
(defun m^ (A B) (element-wise-matrix #'expt A B))
(defun m= (A B) (element-wise-matrix #'= A B))
(defun element-wise-scalar-vector (fn A c )
  (let* ((len (array-total-size A))
         (m   (car (array-dimensions A)))                                  
         (B   (make-array `(,m) :element-type 'single-float )))
    
    (loop for i from 0 to (1- len) do
          (setf (row-major-aref B i) 
                (funcall fn (row-major-aref A i) c)))
    B))

(defun .v^ (A c)
  (let*
      ((len (array-total-size A))
       (m   (car (array-dimensions A)))                                  
       (B   (make-array `(,m) :element-type 'single-float )))
    
    (loop for i from 0 to (1- len) do
          (setf (row-major-aref B i) 
                                        ;swapped A c to c A
                
                (funcall  #'expt c (row-major-aref A i) )
                ))
    B))

(defun .v+ (A c) (element-wise-scalar-vector #'+ A c ))
(defun .v- (A c) (element-wise-scalar-vector #'- A c ))
(defun .v* (A c) (element-wise-scalar-vector #'* A c))
(defun .v/ (A c) (element-wise-scalar-vector #'/ A c))
(defun .v= (A c) (element-wise-scalar-vector #'= A c))

(defun element-wise-scalar (fn A c)
  (let* ((len (array-total-size A))
         (m   (car (array-dimensions A)))
         (n   (cadr (array-dimensions A)))
         (B   (make-array `(,m ,n) :element-type 'single-float )))
    
    (loop for i from 0 to (1- len) do
          (setf (row-major-aref B i) 
                (funcall fn
                         (row-major-aref A i)
                         c)))
    B))

(defun =_1_or_0(one two)
  "convert output from = to 1's and 0's "
  (T_F-to-1_0 (= one two)))

(defun T_F-to-1_0(tf)
  "convert output from = to 1's and 0's "
  (if tf 1.0 0.0 ))

(defun .- (A c) (element-wise-scalar #'- A c))
(defun .= (A c) (element-wise-scalar #'=_1_or_0 A c))

;; Element wise of column vector and row vector:
(defun column-row-element-wise-op (fn col_vec row_vec)
  (loop
    with m = (array-dimension col_vec 0)
    with n = (array-dimension row_vec 0)    
    with c = (make-array `(,m ,n) :element-type 'single-float)
    for i below m do
    (let ((col_element (aref col_vec i)))
      (loop for k below n do
            (setf (aref c i k ) (funcall fn (aref row_vec k) col_element)))
      (setf col_element (aref col_vec i)))
    finally (return c)))

(defun .e+ (C R) (column-row-element-wise-op #'+ C R))

(defun matrix-mul (matrix vector dest)
  "Multiply MATRIX by VECTOR putting the result into DEST.
Optimized for DOUBLE-FLOAT vectors and matrices"
  (declare (type (simple-array single-float (* *)) matrix)
           (type (simple-array single-float *) vector )
           (type (simple-array single-float *) dest )
           (optimize (speed 3) (debug 0) (safety 0)))
  (destructuring-bind (rows cols) (array-dimensions matrix)
    (dotimes (i rows)
      
      (setf (aref dest i)
            (loop for j below cols sum (the single-float
                                            (* (aref matrix i j) (aref vector j))))))))

(defun dest-matrix (matrix)  "creates destination matrix for matrix-mul"
  (make-array (list (array-dimension matrix 1)) :element-type 'single-float :initial-element  0.0))

;; Matrix Multiplication
(defun mmult (a b)
  (loop
    with m = (array-dimension a 0) 
    with n = (array-dimension a 1)
    with l = (array-dimension b 1)
    with c = (make-array (list m l):element-type 'single-float)
    for i below m do
    (loop for k below l do
          (setf (aref c i k)
                (loop for j below n
                      sum (* (aref a i j)
                             (aref b j k)))))
    finally (return c)))

(defun vmult2 (fn v1 v2)
  (loop
    with m = (array-dimension v1 0)
    with c = (make-array (list m) :element-type 'single-float )
    for i below m do

    (setf (aref c i) (funcall fn (aref v1 i) (aref v2 i)))
    finally (return c)))

(defun vmult (fn v1 v2)  "applying two 1D arrays to operations" (map 'vector fn v1 v2))

(defun v+ (v1 v2) (vmult #'+ v1 v2))
(defun v- (v1 v2) (vmult #'- v1 v2))
(defun v* (v1 v2) (vmult2 #'* v1 v2))
(defun v/ (v1 v2) (vmult #'/ v1 v2))
(defun v^ (v1 v2) (vmult #'expt v1 v2))
(defun v= (v1 v2) (vmult #'= v1 v2))

(defun array-min (array )
  "Find the minimum in array."
  (reduce-multidimensional-array #'min array))

(defun reduce-multidimensional-array (fn arr)
  (reduce fn  (array-storage-vector arr)))

;; $A$2:A16>A17-180
(defun Xs-compare-to-last-x (fn Xs)
  (if (< (length Xs) 2)      
      0.0      ; so first weight is 0.0, there's no calc really
      (let* ((len (length Xs))
             (last_x (aref Xs (- len 1))) 
             (sub_Xs (subseq Xs 0 (- len 1 ))))
        (loop
          with m = (array-dimension sub_Xs 0)
          with c = (make-array (list m) :element-type 'single-float )
          for i below m do
          (setf (aref c i) (funcall fn last_x (aref sub_Xs i)))
          finally (return c)))))

(defun horses-compare-to-last-horse (horses) (Xs-compare-to-last-x #'(lambda (last_horse x) (if (= x last_horse) 1.0 0.0)) horses))

(defun dates-compare-to-last-date (dates) (Xs-compare-to-last-x #'(lambda (last_date x) (if (> x (- last_date 180)) 1.0 0.0)) dates))

;; Selects columns for weight function
;; indexed from 1 not 0, meaning col1 == when i equal 1
(defun select-dates-for-weight (full_dates i)
  (subseq full_dates 0 (+ i 1)))
;; indexed from 1 not 0, meaning col1 == when i equal 1
(defun select-positions-for-position (full_positions i)
  (subseq full_positions 0 i))
;; indexed from 1 not 0, meaning col1 == when i equal 1
(defun select-horses-for-horse (full_horses i)
  (subseq full_horses 0 (+ i 1)))

(defun select-row-for-sum-product (row i)
  (subseq row 0 (+ i 1))) 

;; Excel code: EXP(-(MMULT(G17:U17,--(COLUMN(G17:U17)+TRANSPOSE(COLUMN(G17:U17))-2*MIN(COLUMN(G17:U17))=COLUMNS(G17:U17)-1)))/75)
;; COLUMNS(array) returns how many columns in range
;; COLUMN(array returns the index of each (1 2 3 4 5 ..)
(defun expt-mat-multi (row ) ;(row_matrix col_ref_nums)
  (let ((euler (exp 1)))
    (.v^
     (.v*
      (.v/ (reverse row) 75 )
      -1 )
     euler )))

(defun the-sum-product (A B C D)
  (let ((product (v* (v* ( v* A B) C) D)))    
    (reduce-multidimensional-array #'+ product)))

(defun calc-jul-pos-hor-row-for-sum-prod (full_julians full_positions full_horses days_since_LS i)
  (list (dates-compare-to-last-date (select-dates-for-weight full_julians (- i 1)))
        (select-positions-for-position full_positions (- i 1))
        (horses-compare-to-last-horse (select-horses-for-horse full_horses (- i 1) ))
        (select-row-for-sum-product (last-start-days-ago-row i full_horses full_julians days_since_LS) (- i 2))))

;; Input: (calc-jul-pos-hor-row-for-sum-prod (list full_julians full_positions full_horses days_since_LS row#))
(defun Sum-Product (DPHR_list) 
  (the-sum-product (first DPHR_list)(second DPHR_list) (third DPHR_list) (expt-mat-multi (fourth DPHR_list)))) 
