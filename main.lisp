(ql:quickload :array-operations)
(use-package :array-operations)
(Load "c:/Users/mike/Dropbox/RWA-CSV/weights.lisp")
(Load "c:/Users/mike/Dropbox/RWA-CSV/rwa.lisp")
(defparameter *the_csv_file* (getCSV2))
(defparameter *the_csv_file_length* (length *the_csv_file*))
(defparameter *sorted-d-j-p-h* (sort-d-j-p-h(d-p-h->d-j-p-h *the_csv_file*))) ;; (subseq (getcsv2) 1)
(defparameter *dates* (get-column *sorted-d-j-p-h* 0))
(defparameter *julians* (get-column *sorted-d-j-p-h* 1))
(defparameter *positions* (get-column *sorted-d-j-p-h* 2))
(defparameter *horses* (get-column *sorted-d-j-p-h* 3))
(defparameter *days_since_LS*
      (array-operations:stack 0 #(0.0) (if-and-2-func #'are-side-by-side-elem-eq (cutBehind *horses* 1) 1 #'diff-julian-dates (cutFront *julians* 1))))
(defun get-array-sum-product ()
 (loop   
   for row from 2 to *the_csv_file_length*
   do (print (Sum-Product (calc-jul-pos-hor-row-for-sum-prod *julians* *positions* *horses* *days_since_LS* row)))))
