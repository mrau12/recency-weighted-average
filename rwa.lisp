(Load "c:/Users/mike/Dropbox/RWA-CSV/rwa-csv.lisp")
(load "c:/Users/mike/Dropbox/RWA-CSV/rwa-dates.lisp")
(load "c:/Users/mike/Dropbox/RWA-CSV/rwa-sort.lisp")

(eval-when (:compile-toplevel :load-toplevel :execute)  (ql:quickload :trivia))
(ql:quickload "parse-float")
(use-package :parse-float)

;;; The main data we are working with ( ... ("28/03/2014" "3" "5") ("13/03/2014" "6" "5") ("28/03/2014" "3" "5") ... )
(defparameter *date-position-horse* (getCSV))

;;; Raw string form of dates #(("20/02/2014" "4" "1") ("13/03/2014" "2" "1") ("28/03/2014" "4" "1") ... )
(defun get-dates () (map 'vector #'first *date-position-horse*))

;;; Raw positions
(defun get-positions () (map 'vector #'second *date-position-horse*))

;;; Raw horses
(defun get-horses () (map 'vector #'third *date-position-horse*))

(defun /-split (string)
  (loop for start = 0 then (1+ finish)
        for finish = (position #\/ string :start start)
        collecting (subseq string start finish)
        until (null finish)))
;;; The definition dd/mm/yyyy->3str_list
;;; Input: ("20/02/2014" "13/03/2014" "28/03/2014" "20/03/2014" "13/03/2014" "28/03/2014" "13/03/2014" "28/03/2014" ...)
;;; Output: (("20" "02" "2014") ("13" "03" "2014") ("28" "03" "2014") ("20" "03" "2014")("13" "03" "2014") ("28" "03" "2014")
;;; ("20" "02" "2014") ("13" "03" "2014") (..))
(defun dd/mm/yyyy->3str_list (dates)
  (map 'vector (lambda (x) (/-split x)) dates))

;;; Convert sub-Lists of strings to sub-Arrays of integers
;;; Input #(("20" "02" "2014") ("13" "03" "2014") ("28" "03" "2014") ("20" "03" "2014"))
;;; Output #(#(20 2 2014) #(13 3 2014) #(28 3 2014) #(20 3 2014) #(13 3 2014) #(28 3 2014))
(defun 3str_list->3str_array (arr-of-3strlists)
  (map 'vector #'column-parse-float arr-of-3strlists))

;;; Date in Julian Date format
;;; Input: #(#(20 2 2014) #(13 3 2014) #(28 3 2014) #(20 3 2014) #(13 3 2014) #(28 3 2014))
;;; Output: (2456698.5 2456719.5 2456734.5 2456726.5 2456719.5 2456734.5 2456698.5 ...)
(defun ddmmyyyy->julian (dates)
  (map 'vector (lambda (dd_mm_yyyy) (julianday (aref dd_mm_yyyy 0) (aref dd_mm_yyyy 1) (aref dd_mm_yyyy 2))) dates))

;;; Input: ("4" "2" "4" "1" "1" "2" "3" "3" "1" "2" "4" "5" "5" "3" "6" "3" "6" "3")
;;; Output: (4.0 2.0 4.0 1.0 1.0 .0 3.0 3.0 1.0 2.0 4.0 5.0 5.0 3.0 6.0 3.0 6.0 3.0)
(defun column-parse-float(arr)
  (map 'vector (lambda (x) (nth-value 0 (parse-float x))) arr))

;;; Mapcar for array - https://stackoverflow.com/questions/4116069/lisp-multidimensional-array-elementwise-operations
(defun array-map (function &rest arrays)
  "Maps the function over the arrays. Assumes that all arrays are of the same dimensions. Returns a new result array of the same dimension."
  (flet ((make-displaced-array (array)
           (make-array (reduce #'* (array-dimensions array))
                       :displaced-to array)))
    (let* ((displaced-arrays (mapcar #'make-displaced-array arrays))
           (result-array (make-array (array-dimensions (first arrays))))
           (displaced-result-array (make-displaced-array result-array)))
      (declare (dynamic-extent displaced-arrays displaced-result-array))
      (apply #'map-into displaced-result-array function displaced-arrays)
      result-array)))

;;; New variable julian dates added to string date in *date-position-horse* for calculations
;;;
;;; Input: Four Arrays:
;;; ("20/02/2014" "13/03/2014" "28/03/2014" "20/03/2014")
;;; (2456698.5 2456719.5 2456734.5 2456726.5 2456719.55)
;;; (4 2 4 1 1)
;;; (1 1 1 2 2)
;;; Output: (("20/02/2014" 2456698.5 4 1) ...) Turns columns into rows
(defun d-p-h->d-j-p-h (d-p-h)
  (array-map #'vector
          (map 'vector #'first d-p-h)
          (ddmmyyyy->julian(3str_list->3str_array(dd/mm/yyyy->3str_list(map 'vector #'first d-p-h))))
          (column-parse-float (map 'vector #'second d-p-h))
          (column-parse-float (map 'vector #'third d-p-h))))

;;; Sort: date-julian-position-horse array. Horses sort, holding horse stable, then dates.
;;; Input: (("date1" large_float1 pos_int1 hor_int1)  ("date2" large_float2 pos_int2 hor_int2) ... ("dateN" large_floatN pos_intN hor_intN))
;;; Output: Sorts all hor_int then hold previous horse sort stable and sort dates 
(defun sort-d-j-p-h (d-j-p-h)  
  (sort d-j-p-h (and-then 'horse< 'date<)))

;;; Julian dates as simple vector
;;; Input:
;;; date-julian-position-horse array
;;; column starting from 0 to return(the first column is indicated by 0)
;;; Output:
;;; julian array
(defun get-column (d-j-p-h col)
  (let ((new-array (make-array (array-total-size d-j-p-h) )))
        (dotimes (i (array-total-size d-j-p-h)) (setf (row-major-aref new-array i) (aref (row-major-aref d-j-p-h i) col)))
         new-array))

;;; mimics horse compare throughout nth-last-start processy
;;; Example: n = 1 and  #(1 2 3 4 5) ---> remove head ----> #(2 3 4 5) then compare both to each other,
;;; its the same  as comparing the #(1 2 3 4 5) pairs to each other. If equal 1 else NIL
;;; Example: Example: n = 2 and  #(1 2 3 4 5) ---> remove 2 elem ----> #(3 4 5) then compare both to each other,
(defun are-side-by-side-elem-eq (vec n) (map 'vector #'equal vec (subseq vec n)))

;;; #( 1 3 1 4 5 6 4) by #'cddr --> #(1 1 5 4) = '(1 NIL NIL)
;(defun collect-nth-elem-compare-neighbours (arr pattern) (are-side-by-side-elem-eq (collect-arr-with-index-list (list-indices (create-full-index-list arr) pattern) arr)))

;;; Trying to replicate the functionality of excel datedif. Its not very well documented - so quite a bit trial and error
(defun datedif (j1 j2)
  (if (and j1 j2 (plusp (- j2 j1)))
      (- j2 j1)
      0.0)) 

;;; #(2456698.5 2456719.5 2456734.5 2456719.5 2456726.5 2456734.5 2456698.5) --> #(-21 -15 15 -7 -8 36)
(defun diff-julian-dates (julian-vec) (map 'vector #'datedif julian-vec (subseq julian-vec 1)))

;; Straight conversion =IF(C3=C2, DATEDIF( A3,A4,"d"),0)
;; Input:
;; horses-vec and the compare pattern
;; julian-vec 
(defun days-since-ls2 (horses-vec julian-vec skip-pattern)
  (map 'vector
       #'(lambda (tru julian-diff) (if tru julian-diff 0.0))
       (are-side-by-side-elem-eq horses-vec skip-pattern)       
       (diff-julian-dates julian-vec)))

(defun cutFront (arr numToCut) (subseq arr numToCut))

(defun cutBehind (arr numToCut) (subseq arr 0 (- (length arr) numToCut )))

;;; Straight conversion =IF(C3=C2, DATEDIF( A3,A4,"d"),0)
;;; (if-and-2-func #'collect-jumped-elem-compare-neighbours #'diff-julian-dates (stagger 1 3) 1)
(defun if-and-2-func (fn staggered-horses skippattern gn staggered-julians)
  (map 'vector
       #'(lambda (firstout secondout) (if firstout secondout 0.0))
       (funcall fn staggered-horses skippattern)
       (funcall gn staggered-julians)))

;;; Input: array numbers are less than number
;;; Output: 0 or 1
(defun <NUM (daysSinceLSArr NUM)
  (let ((my-bool-array (make-array (length daysSinceLSArr))))
      (dotimes (i (length daysSinceLSArr)) (setf (aref my-bool-array i) (if (< (aref daysSinceLSArr i) NUM) 1.0 0.0)))
    my-bool-array))

;;; =IF(AND(D3<61,C3=C2),E2+1,1)
(defun runs_since_spell (horsesEqualArr daysSinceLSArr)
  (let ((count 1))
    (map 'vector #'(lambda (horseIs= day<61 ) (if (and horseIs= day<61) (progn (setf count (1+ count)) count)  (progn (setf count 1) count) )) horsesEqualArr daysSinceLSArr)))

;;; =IF(C3=C2,F2+1,1)
(defun total_runs (horsesEqualArr)
  (let ((count 1))
    (map 'vector #'(lambda (horseIs=) (if horseIs= (progn (setf count (1+ count)) count)  (progn (setf count 1) count) )) horsesEqualArr)))

;;; =IF(AND(C3=C2,D2<180),A3-A2,0) (creates one element)
(defun 1st_last_start_element (horses dateDiff daysSinceLS )
   (trivia:match horses
    ((trivia:guard x (eq 1.0 x)); aka True
     (trivia:match daysSinceLS
       ((trivia:guard x (< x 180.0)) dateDiff)      
       (_ 0.0)))
     (_ 0.0) ))

;;; =IF(AND(C3=C1,G3+A2-A1<180,G3<>0),G3+A2-A1,0)
(defun Nth_last_start_element (horses dateDiff n-1thLastStartDay)
  (trivia:match horses
    ((trivia:guard x (eq 1.0 x)); aka True
     (trivia:match n-1thLastStartDay
       ((trivia:guard x (and (not(= x 0.0)) (< (+ dateDiff x) 180.0) ))
        (+ dateDiff n-1thLastStartDay) )
       (_ 0.0)))
    (_ 0.0)))

;;; =IF(AND(C_=C_,D_<180),A_-A_,0) (create entire column)
(defun get_1st_last_start (horses-vec date-diff-vec daysSinceLS-vec)
  (map 'vector
       #'1st_last_start_element
       horses-vec
       date-diff-vec
       daysSinceLS-vec))

;;; =IF(AND(C_=C_,D_<180),A_-A_,0) (create entire column)
;;; horses going to change shape
(defun get_Nth_last_start_element (horses dateDiff prev_last_start)
  (map 'vector
         #'Nth_last_start_element
         horses
         dateDiff
         prev_last_start))

(defun collect-jumped-elem-compare-neighbours (arr jumps) (are-side-by-side-elem-eq arr jumps))

;;TODO: convert to macro
(defun prepend-zeros-to-columns (column)
  (trivia:match column    
    ((vector) (vector 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0))
    ((vector x1) (vector 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 x1))
    ((vector x1 x2) (vector 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 x1 x2))
    ((vector x1 x2 x3) (vector 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 x1 x2 x3))
    ((vector x1 x2 x3 x4) (vector 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 x1 x2 x3 x4))
    ((vector x1 x2 x3 x4 x5) (vector 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 x1 x2 x3 x4 x5))
    ((vector x1 x2 x3 x4 x5 x6) (vector 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 x1 x2 x3 x4 x5 x6))
    ((vector x1 x2 x3 x4 x5 x6 x7) (vector 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 x1 x2 x3 x4 x5 x6 x7))
    ((vector x1 x2 x3 x4 x5 x6 x7 x8) (vector 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 x1 x2 x3 x4 x5 x6 x7 x8))
    ((vector x1 x2 x3 x4 x5 x6 x7 x8 x9) (vector 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 x1 x2 x3 x4 x5 x6 x7 x8 x9))
    ((vector x1 x2 x3 x4 x5 x6 x7 x8 x9 x10) (vector 0.0 0.0 0.0 0.0 0.0 0.0 0.0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10))
    ((vector x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11) (vector 0.0 0.0 0.0 0.0 0.0 0.0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11))
    ((vector x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12) (vector 0.0 0.0 0.0 0.0 0.0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12))
    ((vector x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13) (vector 0.0 0.0 0.0 0.0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13))
    ((vector x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14) (vector 0.0 0.0 0.0  x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14))
    ((vector x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15) (vector 0.0 0.0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15))
    ((vector x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15 x16) (vector 0.0 x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x15 x16))
    ((vector x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x16 x17) (vector x1 x2 x3 x4 x5 x6 x7 x8 x9 x10 x11 x12 x13 x14 x16 x17))))


;; (defun append-vec-to-zeros (vec)
;;   (let ((len_vec  (length vec))
;;         (vec_zeros (make-array 17 :initial-element 0.0)))
;;     (if (eq 0 len_vec) vec_zeros
;;         (do j (- 17 len_vec) 17 
;;           (setf (aref vec_zeros j) )
;;           )
;;         )))

