;;;       FIND DIFFERENCE IN DATES by converting to Julian date
(defun initialMonthCheck (year month); calcs yearp and monthp
  (cond ((or (= month 1 ) (= month 2) )
         (values-list (list (- year 1) (+ month 12)) ))  
        (t
         (values-list (list year month)))))

(defun isYearLessThan1582 (year month day)
  (or
   (< year 1582)
   (and
    (= year 1582) (< month 10))
   (and
    (= year 1582) (= month 10) (< day 15))))

                                       
(defun calcB (year month day yearp) ; calculate B by comparing current date to Gregorian date
  (cond ((isYearLessThan1582 year month day) 0); B is 0
        (t (truncB yearp))))

(defun truncYearp (yearp)  (truncate (/ yearp 100)))

 (defun truncB (yearp) (let ((tyearp))
                        (setf (values tyearp) (truncYearp yearp))
                        (- 2 (+ tyearp (/ tyearp 4)))))

                                       
(defun calcC (yearp)  ; calculate C
  (cond
    ((< yearp 0) (- (truncC yearp) 0.75))
    (t (truncC yearp))))


(defun truncC (yearp)
  (truncate (* yearp 365.25)))

(defun calcD (monthp)
  (truncate (* 30.6001 (+ 1 monthp))))

;;; Converts dd mm yyyy -> nnnnnn.n
(defun julianDay (day month year)
  (let ((yearp nil) (monthp nil))
    (setf (values yearp monthp) (initialMonthCheck year month))
    (+ 1720994.5 day (calcD monthp) (calcC yearp) (calcB year month day yearp))))

;;; (1 2 3 4) ----difference between neighbours----> (1 1 1)
(defun j2-minus-j1 (julian-lst)  
  (loop for (j1 j2) on julian-lst while j2 collect (- j2 j1)))
