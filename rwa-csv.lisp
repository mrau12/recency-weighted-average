(defpackage :RWA (:use :cl ))
;;      LOAD CSV
(eval-when (:compile-toplevel :load-toplevel :execute)  (ql:quickload "cl-csv"))

(defun col-to-vec (columnfn data) (apply #'vector (mapcar columnfn data)) )

;(defun getCSV () (rest(cl-csv:read-csv #P "/home/mike/Dropbox/RWA-CSV/test.csv"))) ;linux


(defun getCSV-thousand () (let ((data (rest (cl-csv:read-csv #P"c:/Users/mike/Dropbox/RWA-CSV/thousand_test.csv" :separator #\,))))
                            (apply #'vector data)))
                   
(defun getCSV () (let ((data (rest (cl-csv:read-csv #P"C:/Users/mike/Dropbox/RWA-CSV/test.csv" :separator #\,))))
                   (apply #'vector data)))

;; MAKE SURE CORRECT .CSV format
(defun getCSV2 () (rest (cl-csv:read-csv #P"c:/Users/mike/Dropbox/RWA-CSV/Input3.csv"
                                         :separator #\,
                                         :map-fn #'(lambda (x) (apply #'list (list (nth 0 x) (nth 1 x) (nth 3 x))) ))))

;; your example executed using the new function:
;;(nth-csv-row #P"file.csv" 5)
(defun nth-csv-row (csv-path n &rest read-csv-row-parameters)
  "Return nth line of a csv file, parsed."
  (with-open-file (stream csv-path)
    (loop for x from 1 below n
          do (cl-csv:read-csv-row stream))
    (apply #'cl-csv:read-csv-row stream read-csv-row-parameters)))

