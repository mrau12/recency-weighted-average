
;;; Sort the date then horse ("13/03/2014" 2456719.5 6 5) ("28/03/2014" 2456734.5 3 5))
(defun and-then (original-predicate next-predicate)
  "Returns a new predicate constructed from ORIGINAL-PREDICATE and
NEXT-PREDICATE.  The new predicate compares two elements, x and y, by
checking first with ORIGINAL-PREDICATE.  If x is less than y under
ORIGINAL-PREDICATE, then the new predicate returns true.  If y is less
than x under ORIGINAL-PREDICATE, then the new predicate returns false.
Otherwise, the new predicate compares x and y using NEXT-PREDICATE."
  (lambda (x y)
    (cond
      ((funcall original-predicate x y) t)
      ((funcall original-predicate y x) nil)
      (t (funcall next-predicate x y)))))

(Defun term-date (term)
  (aref term 1));second

(defun date< (term1 term2)
  (< (term-date term1)
     (term-date term2)))

(defun term-horse (term)
  (aref term 3));fourth

(defun horse< (term1 term2)
  (< (term-horse term1)
     (term-horse term2)))
